<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TblBarang;
use App\TblBrgMasuk;
use App\TblBrgPenjualan;
use App\TblDetailBrgMasuk;
use App\TblDetailPenjualan;
use App\TblDistributor;
use App\TblJenis;
use App\TblPetugas;
use App\User;

class DetailBrgMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['detailbarangmasuk'] = TblDetailBrgMasuk::all();
        $data['allbarangmasuk'] = TblBrgMasuk::all();
        $data['allbarang'] = TblBarang::all();
        return view('admin.detailbarang')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newdetailbarangmasuk = new TblDetailBrgMasuk;

        $newdetailbarangmasuk->nonota = $request->input('nonota');
        $newdetailbarangmasuk->kodebarang = $request->input('kodebarang');
        $newdetailbarangmasuk->jumlah = $request->input('jumlah');
        $newdetailbarangmasuk->subtotal = $request->input('subtotal');

        $newdetailbarangmasuk->save();
        return redirect('detailbarangmasuk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
