<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TblBarang;
use App\TblBrgMasuk;
use App\TblBrgPenjualan;
use App\TblDetailBrgMasuk;
use App\TblDetailPenjualan;
use App\TblDistributor;
use App\TblJenis;
use App\TblPetugas;
use App\User;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['allbarang'] = TblBarang::all();
        $data['alljenis'] = TblJenis::all();
        return view('admin.barang')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addbarang = new TblBarang;

        $addbarang->kodebarang = $request->input('kodebarang');
        $addbarang->namabarang = $request->input('namabarang');
        $addbarang->kodejenis = $request->input('kodejenis');
        $addbarang->harganet = $request->input('harganet');
        $addbarang->hargajual = $request->input('hargajual');
        $addbarang->stok = $request->input('stok');

        $addbarang->save();
        return redirect('barang')->with('alertbarang', $request->input('namabarang'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
