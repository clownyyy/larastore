@extends('layouts.app')

@section('content')
<div class="row">
    @foreach($allbarang as $bar)
        <div class="col-md-3">
            <div class="card">
                <img src="{{asset('template/image/default.png')}}" class="card-img-top" style="" alt="Card image">
                <div class="card-body">
                    <h4 class="card-title">{{$bar->namabarang}}</h4>
                    <p class="card-text" style="color: lime">Rp. {{$bar->hargajual}}</p>
                    <p class="card-text">Lorem ipsum dolor ist amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet</p>
                    <a href="#" class="btn btn-primary">Detail</a>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
