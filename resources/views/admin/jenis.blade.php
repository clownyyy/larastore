@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box">
			<div class="box-header">
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#AddJenis"><i class="fa fa-plus"></i> New Jenis</button>
			</div>
			<div class="box-body">
				<table class="table table-bordered" id="Jenis">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Jenis</th>
							<th>Jenis</th>
						</tr>
					</thead>
					<tbody>
						@foreach($alljenis as $p)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$p->kodejenis}}</td>
							<td>{{$p->jenis}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="AddJenis" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Jenis</h4>
			</div>
			<form action="{{route('jenis.store')}}" method="post">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<h5>Kode Jenis</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="kodejenis" value="{{rand()}}" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Jenis</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="jenis">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#Jenis').DataTable();
</script>
@endsection