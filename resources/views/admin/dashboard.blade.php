@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-3">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>{{$allbarang->count()}}</h3>
						<p>Jumlah Barang</p>
					</div>
					<div class="icon">
						<i class="fa fa-truck"></i>
					</div>
					<a href="#" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="small-box bg-red">
					<div class="inner">
						<h3>{{$allpetugas->count()}}</h3>
						<p>Jumlah Petugas</p>
					</div>
					<div class="icon">
						<i class="fa fa-wrench"></i>
					</div>
					<a href="#" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{$alldistributor->count()}}</h3>
						<p>Jumlah Distributor</p>
					</div>
					<div class="icon">
						<i class="fa fa-users"></i>
					</div>
					<a href="#" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>{{$alluser->count()}}</h3>
						<p>Jumlah Pengguna</p>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
					<a href="#" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="box box-primary">
					<div class="box-header">Data Barang</div>
					<div class="box-body">
						<table class="table table-bordered" id="barang">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Barang</th>
									<th>Kode Jenis</th>
									<th>Harga Net</th>
									<th>Harga Jual</th>
									<th>Stok</th>
								</tr>
							</thead>
							<tbody>
								@foreach($allbarang as $brg)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$brg->namabarang}}</td>
									<td>{{$brg->kodejenis}}</td>
									<td>{{$brg->harganet}}</td>
									<td>{{$brg->hargajual}}</td>
									<td>{{$brg->stok}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box box-warning">
					<div class="box-header">Pengumuman</div>
					<div class="box-body">
						<div class="chat" id="chat-box">
							@foreach($allpengumuman as $peng)
							<div class="item">
								<img src="{{asset('template/dist/img/user2-160x160.jpg')}}">
								<p class="message">
									{{$peng->pengumuman}}<br>
									<small style="color: silver">By: {{$peng->author}}</small>
									<small class="text-muted pull-right">{{$peng->created_at}}</small>
								</p>
							</div>
							@endforeach
						</div>
					</div>
					<div class="box-footer">
						<form method="post" action="{{route('pengumuman.store')}}">
							@csrf
			                <div class="input-group">
			                  <input type="hidden" name="author" value="{{ucfirst(Auth::user()->name)}}">
			                  <input class="form-control" placeholder="Type Announce" name="pengumuman">
			                  <div class="input-group-btn">
			                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
			                  </div>
			                </div>
			            </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#barang').DataTable();
	$('#petugas').DataTable();
	$('#user').DataTable();
</script>
@endsection