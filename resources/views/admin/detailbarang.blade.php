@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box">
			<div class="box-header">
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#NewDetailBarangMasuk"><i class="fa fa-plus"></i> New Detail Barang</button>
			</div>
			<div class="box-body">
				<table class="table table-bordered" id="DetailBarangMasuk">
					<thead>
						<tr>
							<th>No</th>
							<th>No Nota</th>
							<th>Kode Barang</th>
							<th>Jumlah</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<tbody>
						@foreach($detailbarangmasuk as $d)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$d->nonota}}</td>
							<td>{{$d->kodebarang}}</td>
							<td>{{$d->jumlah}}</td>
							<td>{{$d->subtotal}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="NewDetailBarangMasuk" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Barang Masuk</h4>
			</div>
			<form action="{{route('detailbarangmasuk.store')}}" method="post">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<h5>No Nota</h5>
						</div>
						<div class="col-md-9">
							<select class="form-control" name="nonota">
								@foreach($allbarangmasuk as $barmas)
								<option>{{$barmas->nonota}}</option>
								@endforeach()
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Kode Barang</h5>
						</div>
						<div class="col-md-9">
							<select class="form-control" name="kodebarang">
								@foreach($allbarang as $bar)
								<option value="{{$bar->kodebarang}}">{{$bar->kodebarang}} | {{$bar->namabarang}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Jumlah</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="jumlah">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Subtotal</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="subtotal">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#DetailBarangMasuk').DataTable();
</script>
@endsection