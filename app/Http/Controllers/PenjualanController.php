<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TblBarang;
use App\TblBrgMasuk;
use App\TblBrgPenjualan;
use App\TblDetailBrgMasuk;
use App\TblDetailPenjualan;
use App\TblDistributor;
use App\TblJenis;
use App\TblPetugas;
use App\User;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['allpenjualan'] = TblBrgPenjualan::all();
        $data['allpetugas'] = TblPetugas::all();
        return view('admin.penjualan')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addpenjualan = new TblBrgPenjualan;

        $addpenjualan->nofaktur = $request->input('nofaktur');
        $addpenjualan->tglpenjualan = $request->input('tglpenjualan');
        $addpenjualan->idpetugas = $request->input('idpetugas');

        $addpenjualan->save();
        return redirect('penjualan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
