@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box">
			<div class="box-header">
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#AddDistributor"><i class="fa fa-plus"></i> New Distributor</button>
			</div>
			<div class="box-body">
				<table class="table table-bordered" id="Distributor">
					<thead>
						<tr>
							<th>No</th>
							<th>ID Distributor</th>
							<th>Nama Distributor</th>
							<th>Alamat</th>
							<th>Kota Asal</th>
							<th>Email</th>
							<th>No Telp</th>
						</tr>
					</thead>
					<tbody>
						@foreach($alldistributor as $d)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$d->iddistributor}}</td>
							<td>{{$d->namadistributor}}</td>
							<td>{{$d->alamat}}</td>
							<td>{{$d->kotaasal}}</td>
							<td>{{$d->email}}</td>
							<td>{{$d->telpon}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="AddDistributor" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Distributor</h4>
			</div>
			<form action="{{route('distributor.store')}}" method="post">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<h5>ID Distributor</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="iddistributor" placeholder="ID Distributor" readonly value="{{rand()}}">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Nama Distributor</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="namadistributor" placeholder="Nama Distributor" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Alamat</h5>
						</div>
						<div class="col-md-9">
							<textarea class="form-control" rows="3" name="alamat" placeholder="Alamat" required></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Kota Asal</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="kotaasal" placeholder="Kota Asal" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Email</h5>
						</div>
						<div class="col-md-9">
							<input type="email" class="form-control" name="email" placeholder="Email" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Nomor Telepon</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="telpon" placeholder="Nomor Telepon" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#Distributor').DataTable();
</script>
@endsection