@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box">
			<div class="box-header">
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#AddPenjualan"><i class="fa fa-plus"></i> New Penjualan</button>
			</div>
			<div class="box-body">
				<table class="table table-bordered" id="penjualan">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor Faktur</th>
							<th>Tanggal Penjualan</th>
							<th>ID Petugas</th>
						</tr>
					</thead>
					<tbody>
						@foreach($allpenjualan as $p)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$p->nofaktur}}</td>
							<td>{{$p->tglpenjualan}}</td>
							<td>{{$p->idpetugas}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="AddPenjualan" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Penjualan</h4>
			</div>
			<form action="{{route('penjualan.store')}}" method="post">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<h5>Nomor Faktur</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="nofaktur" value="LRS{{rand()}}" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Tanggal Penjualan</h5>
						</div>
						<div class="col-md-9">
							<input type="date" class="form-control" name="tglpenjualan">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>ID Petugas</h5>
						</div>
						<div class="col-md-9">
							<select class="form-control" name="idpetugas">
								@foreach($allpetugas as $pet)
								<option value="{{$pet->idpetugas}}">{{$pet->idpetugas}} | {{$pet->namapetugas}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#penjualan').DataTable();
</script>
@endsection