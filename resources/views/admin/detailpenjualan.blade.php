@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box">
			<div class="box-header">
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#AddDetailPenjualan"><i class="fa fa-plus"></i> New Detail Penjualan</button>
			</div>
			<div class="box-body">
				<table class="table table-bordered" id="DetailPenjualan">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor Faktur</th>
							<th>Kode Barang</th>
							<th>Jumlah</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<tbody>
						@foreach($alldetailpenjualan as $p)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$p->nofaktur}}</td>
							<td>{{$p->kodebarang}}</td>
							<td>{{$p->jumlah}}</td>
							<td>{{$p->subtotal}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#DetailPenjualan').DataTable();
</script>
@endsection