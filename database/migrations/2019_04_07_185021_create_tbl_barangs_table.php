<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_barangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kodebarang')->unsigned();
            $table->string('namabarang');
            $table->integer('kodejenis')->unsigned();
            $table->integer('harganet');
            $table->integer('hargajual');
            $table->integer('stok');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_barangs');
    }
}
