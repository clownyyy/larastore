<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TblBarang;
use App\TblPetugas;
use App\User;
use App\TblDistributor;
use App\Pengumuman;
use App\TblBrgMasuk;
use App\TblBrgPenjualan;
use App\TblDetailBrgMasuk;
use App\TblDetailPenjualan;
use App\TblJenis;
class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['allpetugas'] = TblPetugas::all();
        $data['alluser'] = User::all();
        $data['alldistributor'] = TblDistributor::all();
        $data['allbarang'] = TblBarang::all();
        $data['allpengumuman'] = Pengumuman::all();
        return view('admin.dashboard')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
