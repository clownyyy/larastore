<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBrgMasuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_brg_masuks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nonota')->unsigned();
            $table->date('tglmasuk');
            $table->integer('iddistributor')->unsigned();
            $table->integer('idpetugas')->unsigned();
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_brg_masuks');
    }
}
