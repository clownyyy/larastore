@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box">
			<div class="box-header">
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#AddBarang"><i class="fa fa-plus"></i> New Barang</button>
			</div>
			<div class="box-body">
				<table class="table table-bordered" id="barang">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Barang</th>
							<th>Nama Barang</th>
							<th>Kode Jenis Barang</th>
							<th>Harga Net</th>
							<th>Harga Jual</th>
							<th>Stok</th>
						</tr>
					</thead>
					<tbody>
						@foreach($allbarang as $bar)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$bar->kodebarang}}</td>
							<td>{{$bar->namabarang}}</td>
							<td>{{$bar->kodejenis}}</td>
							<td>{{$bar->harganet}}</td>
							<td>{{$bar->hargajual}}</td>
							<td>{{$bar->stok}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="AddBarang" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Jenis</h4>
			</div>
			<form action="{{route('barang.store')}}" method="post">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<h5>Kode Barang</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="kodebarang" value="{{rand()}}" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Nama Barang</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="namabarang">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Kode Jenis</h5>
						</div>
						<div class="col-md-9">
							<select class="form-control" name="kodejenis">
								@foreach($alljenis as $jen)
								<option value="{{$jen->kodejenis}}">{{$jen->kodejenis}} | {{$jen->jenis}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Harga Net</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="harganet">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Harga Jual</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="hargajual">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Stok</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="stok">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@if(session('alertbarang')){
	<script>
		alert('berhasil menambah barang'+{{ session('alertbarang') }});
	</script>
}
@endif
@endsection
@section('foot-content')
<script>
	$('#barang').DataTable();
</script>
@endsection