@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<div class="content">
		<div class="box box-primary">
			<div class="box-header">Data Barang Masuk
				<button class="pull-right btn btn-primary" data-toggle="modal" data-target="#AddBarangMasuk"><i class="fa fa-plus"></i> New Barang Masuk</button></div>
			<div class="box-body">
				<table class="table table-bordered" id="BarangMasuk">
					<thead>
						<tr>
							<th>No.</th>
							<th>No Nota</th>
							<th>Tanggal Masuk</th>
							<th>ID Distributor</th>
							<th>ID Petugas</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						@foreach($allbarangmasuk as $b)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td>{{$b->nonota}}</td>
							<td>{{$b->tglmasuk}}</td>
							<td>{{$b->iddistributor}}</td>
							<td>{{$b->idpetugas}}</td>
							<td>{{$b->total}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="AddBarangMasuk" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Barang Masuk</h4>
			</div>
			<form action="{{route('barangmasuk.store')}}" method="post">
				@csrf
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<h5>No Nota</h5>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="nonota" value="{{rand()}}" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Tanggal Masuk</h5>
						</div>
						<div class="col-md-9">
							<input type="date" class="form-control" name="tglmasuk">
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>ID Distributor</h5>
						</div>
						<div class="col-md-9">
							<select class="form-control" name="iddistributor">
								@foreach($alldistributor as $dis)
								<option value="{{$dis->iddistributor}}">{{$dis->iddistributor}} | {{$dis->namadistributor}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>ID Petugas</h5>
						</div>
						<div class="col-md-9">
							<select class="form-control" name="idpetugas">
								@foreach($allpetugas as $pet)
								<option value="{{$pet->idpetugas}}">{{$pet->idpetugas}} | {{$pet->namapetugas}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Total</h5>
						</div>
						<div class="col-md-9">
							<input type="number" class="form-control" name="total">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('foot-content')
<script>
	$('#BarangMasuk').DataTable();
</script>
@endsection